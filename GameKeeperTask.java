
public class GameKeeperTask implements Runnable{
	
	GameFrame parent;
   boolean active;
   final int SLEEPTIME = 200; // .2 second
   
   GameKeeperTask(GameFrame pnt)
   {
      parent = pnt;
      active = true;
   }
   
   public void TurnMeOff()
   {
      active = false;
      try
      {
         Thread.sleep(SLEEPTIME+1); // give it time to get the message
      }
      catch (InterruptedException ex)
      {          
      }
   }
   
   public void TurnMeOn()
   {
      active = true;
   }
  
   public void run()
   {
      while (active && !parent.game1.CheckGameOver())
      {
         try
         {
            Thread.sleep(SLEEPTIME);
         }
         catch (InterruptedException ex)
         {          
         }
			parent.SetInput("f");
         parent.lblMessage.setText(parent.game1.GameRefresh(parent.input));
			parent.taDisplay.setText(parent.game1.PrintGrid());
			parent.SetTimeLabel();
      }
      if(parent.game1.CheckGameOver()) {
      	parent.txtInput.setVisible(false);
      	this.active = false;
      }
   }
}


public class Fiend {

	private int row;
	private int col;
	
	public Fiend() {
		
		row = GameEscape.GRID_SIZE_ROW / 2;
		col = GameEscape.GRID_SIZE_COL / 2;
		
	}
	
	public Fiend( int row, int col ) {
		
		if(!SetPosition(row,col))
			this.row = 0;
			this.col = 0;
		
	}
	
	//Accessors
	
	public int GetRow() {
		
		return row;
		
	}
	
	public int GetCol() {
		
		return col;
		
	}
	
	//Mutators
	
	private boolean SetPosition( int row, int col ) {
		
		if( row > GameEscape.GRID_SIZE_ROW - 1 || row < 0 ||
			 col > GameEscape.GRID_SIZE_COL - 1 || col < 0 )
			return false;
		
		this.row = row;
		this.col = col;
		return true;
		
	}
	
	public void RandomMove() {
		
		int new_row;
		int new_col;
		int random_num;
		
		do {
			
			random_num = (int)(Math.random() * 10);

			new_row = this.row;
			new_col = this.col;
			
			//Random move. Note 0 and 9 fiend stays stationary
			switch (random_num) {
				case 1:	new_row -= 1;
							new_col -= 1; 
							break;
				case 2: 	new_row -= 1;
							break;
				case 3: 	new_row -= 1;
							new_col += 1;
							break;
				case 4: 	new_col += 1;
							break;
				case 5: 	new_row += 1;
							new_col += 1;
							break;
				case 6: 	new_row += 1;
							break;
				case 7: 	new_row += 1;
							new_col -= 1;
							break;
				case 8: 	new_col -= 1;
							break;
				default: break;
			}
			
		}while(!SetPosition(new_row,new_col)); //Make sure can move that way
		
	}
	
}


public class EscapeMan {
	
	private int row;
	private int col;
	
	//Constructors
	public EscapeMan() {
		
		row = 0;
		col = 0;
		
	}
	
	public EscapeMan( int row, int col ) {
		
		if(!SetPosition(row,col))
			this.row = 0;
			this.col = 0;
		
	}
	
	//Accessors
	
	public int GetRow() {
		
		return row;
		
	}
	
	public int GetCol() {
		
		return col;
		
	}
	
	//Mutators
	private boolean SetPosition( int row, int col ) {
		
		if( row > GameEscape.GRID_SIZE_ROW - 1 || row < 0 ||
			 col > GameEscape.GRID_SIZE_COL - 1 || col < 0 )
			return false;
		
		this.row = row;
		this.col = col;
		return true;
		
	}
	
	//Sets grid position based on direction entered
	public boolean Move( char dir ) {
		
		int new_row = this.row;
		int new_col = this.col;
		
		switch (dir) {
			case 'a':	new_col -= 1;
							break;
			case 's':	new_row += 1;
							break;
			case 'd': 	new_col += 1;
							break;
			case 'w': 	new_row -= 1;
							break;
			default: 	break;
		}
		
		if(SetPosition(new_row,new_col))
			return true;
		else
			return false;
		
	}

}

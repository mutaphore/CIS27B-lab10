import java.awt.FlowLayout;
import java.awt.event.*;
import javax.swing.*;
import java.util.Date;

public class GameFrame extends JFrame{
	
   public JTextField txtInput;
   public JTextArea taDisplay;
   public JLabel lblMessage;
   public JLabel lblTimer;
   //private JButton btnStart;
   public char input;
	GameEscape game1;
	GameKeeperTask gamekeeper;
	private long time_elapsed;
	private long start_time;
   
   public GameFrame(String title) {
   	
   	super(title);
   	
   	game1 = new GameEscape();
   	
   	// set up layout which will control placement of buttons, etc.
      FlowLayout layout = new FlowLayout(FlowLayout.CENTER, 200, 10);  
      setLayout(layout);
      
      gamekeeper = new GameKeeperTask(this);
      txtInput = new JTextField(1);
      taDisplay = new JTextArea(game1.PrintGrid());
      lblMessage = new JLabel("            ");
      lblTimer = new JLabel("           ");
      taDisplay.setFocusable(false);
   	add(taDisplay);
   	add(lblMessage); 
      add(txtInput);  
      add(lblTimer);
      txtInput.addActionListener(new TextFieldListener());
      
      //Remembers start time
		start_time = new Date().getTime();
		StartGame();
   	
   }
   
   class TextFieldListener implements ActionListener {
   	
   	public void actionPerformed(ActionEvent e)
      {         
   		
   		//Check input
   		if(SetInput(txtInput.getText())) {
   			lblMessage.setText(game1.GameRefresh(input));
   			taDisplay.setText(game1.PrintGrid());
   		}
   		txtInput.setText("");
   	   		
      }
   	
   }
   
   void StartGame()
   {
      // if an existing thread is running, stop it
      StopGame();
      Thread thread = new Thread(gamekeeper);
      gamekeeper.TurnMeOn();
      thread.start();
   }
   
   void StopGame()
   {
   	gamekeeper.TurnMeOff();
   }
   
   void SetTimeLabel()
   {
   	time_elapsed = new Date().getTime() - start_time;
      lblTimer.setText(TimeFormatter(time_elapsed/1000));
   }
   
   //Checks and filters user input string
   public boolean SetInput(String dir){
   	
   	char input_buffer;
   	
   	if(dir.length() > 0)
   		input_buffer = dir.charAt(0);
   	else
   		return false;
		
   	switch (input_buffer) {
   		case 'a': 	input = input_buffer;
   						return true;
   		case 's': 	input = input_buffer;
   						return true;
   		case 'd': 	input = input_buffer;
   						return true;
   		case 'w': 	input = input_buffer;
   						return true;
   		case 'f':	input = input_buffer;
   						return true;
   		default: 	return false;
   	}
      	
   }
   
   //Formats time passed in seconds into MM:SS format
   private String TimeFormatter(long Time_Elapsed) {
   	
   	StringBuffer temp = new StringBuffer();
   	long minutes;
   	long seconds;
   	String minute_digit = "";
   	String second_digit = "";
   	
   	minutes = Time_Elapsed / 60;
   	seconds = Time_Elapsed % 60;
   	
   	if(minutes < 10)
   		minute_digit = "0";
   	if(seconds < 10)
   		second_digit = "0";
   	
   	temp.append(minute_digit + minutes + ":" + second_digit + seconds);
   	
   	return temp.toString();
   	
   }
  
}

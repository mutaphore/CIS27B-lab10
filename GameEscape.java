
public class GameEscape {
	
	public static int GRID_SIZE_ROW = 4;
	public static int GRID_SIZE_COL = 11;
	public static int VALUE_MAN = 1;
	public static int VALUE_FIEND = 2;
	public static int VALUE_DEATH = 3;
	private int[][] grid;
	private EscapeMan jacke;
	private Fiend f1;
	
	public GameEscape() {
		
		grid = new int[GRID_SIZE_ROW][GRID_SIZE_COL];
		
		for(int i = 0; i < GRID_SIZE_ROW; i++)
			for(int j = 0; j < GRID_SIZE_COL; j++)
				grid[i][j] = 0;
		
		jacke = new EscapeMan();
		f1 = new Fiend();
		SetGrid(jacke.GetRow(),jacke.GetCol(),VALUE_MAN);
		SetGrid(f1.GetRow(),f1.GetCol(),VALUE_FIEND);
		
	}
	
	private boolean SetGrid( int row, int col, int val ) {
		
		if( row > GRID_SIZE_ROW - 1 || row < 0 ||
			 col > GRID_SIZE_COL - 1 || col < 0 )
			return false;
		
		grid[row][col] = val;
		return true;
		
	}
	
	public String GameRefresh(char dir) {
		
		String msg = "type a,s,d,w and enter to move";
		int man_prev_row = jacke.GetRow();
		int man_prev_col = jacke.GetCol();
		int fiend_prev_row = f1.GetRow();
		int fiend_prev_col = f1.GetCol();
		
		//Move man based on direction input
		jacke.Move(dir);
		
		//Move fiend randomly
		f1.RandomMove();
		
		//Clear grid of old position
		SetGrid(man_prev_row, man_prev_col, 0);
		SetGrid(fiend_prev_row, fiend_prev_col, 0);
		//Update grid with move
		if(CheckGameOver()) {
			msg = "GAME OVER :(";
			SetGrid(jacke.GetRow(),jacke.GetCol(),VALUE_DEATH);
		}
		else {
			SetGrid(jacke.GetRow(),jacke.GetCol(),VALUE_MAN);
			SetGrid(f1.GetRow(),f1.GetCol(),VALUE_FIEND);
		}
		
		return msg;
		
	}
	
	//Returns a String that contains how the grid looks like right now
	public String PrintGrid() {
		
		StringBuffer temp = new StringBuffer();
		
		for(int i = 0; i < GRID_SIZE_COL; i++)
			temp.append(".");
		
		temp.append("\n");
		
		for(int i = 0; i < GRID_SIZE_ROW; i++) {
			for(int j = 0; j < GRID_SIZE_COL; j++) {
				if(grid[i][j] == 0)
					temp.append(" ");
				if(grid[i][j] == VALUE_MAN)
					temp.append("j");
				if(grid[i][j] == VALUE_FIEND)
					temp.append("I");
				if(grid[i][j] == VALUE_DEATH)
					temp.append("X");
			}
			temp.append("\n");
		}
		
		for(int i = 0; i < GRID_SIZE_COL; i++)
			temp.append(".");
		
		return temp.toString();
		
	}
	
	//Returns true if game is over
	public boolean CheckGameOver() {
		
		if(jacke.GetCol() == f1.GetCol() && jacke.GetRow() == f1.GetRow())
			return true;
		else
			return false;
		
	}
	
}
